﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideController : MonoBehaviour
{
    private static Animator hideControl;
    private static bool holding = false;

    public GameObject arraySource;
    public GameObject logCollider;

    // Start is called before the first frame update
    void Start()
    {
        hideControl = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (hideControl != null)
        {
            if (!hideControl.GetBool("Hide") && holding && (Input.GetKeyDown(KeyCode.E)))
            {
                hideControl.SetBool("Hide", true);
            }
            else if (hideControl.GetBool("Hide") && holding && (Input.GetKeyDown(KeyCode.E)))
            {
                hideControl.SetBool("Hide", false);
            }
        }

        if (canRoll())
        {
            holding = true;
            Destroy(logCollider);
        }
    }

    public bool canRoll()
    {
        bool roll = true;
        for (int i = 1; i < arraySource.GetComponent<PlayerScript>().getCoco().Length; i++)
        {
            if (!arraySource.GetComponent<PlayerScript>().getCoco()[i].gameObject.GetComponent<MeshRenderer>().enabled)
            {
                roll = false;
            }
        }

        if (hideControl != null)
        {
            if (hideControl.GetCurrentAnimatorStateInfo(0).IsName("CocoAnim"))
            {
                roll = false;
            }
        }
       
        return roll;
    }

    public static bool isHolding()
    {
        return holding;
    }

    public void startRoll(bool on, float move)
    {
        if (hideControl != null)
        {
            if ((holding && hideControl.GetBool("Hide")) && on && move > 0)
            {
                hideControl.SetBool("Roll", true);
                hideControl.SetBool("Reverse", false);
            }
            else if ((holding && hideControl.GetBool("Hide")) && on && move < 0)
            {
                hideControl.SetBool("Reverse", true);
                hideControl.SetBool("Roll", false);
            }
            else if (!on)
            {
                hideControl.SetBool("Roll", false);
                hideControl.SetBool("Reverse", false);
            }
        }
    }

    public void downHill(bool on)
    {
        if (hideControl != null)
        {
            if ((holding && !hideControl.GetBool("Downhill")) && on)
            {
                hideControl.SetBool("Downhill", true);
            }
            else if (!on)
            {
                hideControl.SetBool("Downhill", false);
                hideControl.SetBool("Hide", hideControl.GetBool("Hide"));
            }
        }
    }
}
