﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [Tooltip("Movement speed of the player.")]
    public float speed = 5.0f;
    [Tooltip("Jump strength of the player.")]
    public float jumpSpeed = 3.0f;

    public GameObject rollTrigger;
    public GameObject spawnWall;

    private Transform playerTransform;
    private Transform[] cocoArr;

    private float stamina;
    public float maxStamina;

    private int staminaFall;
    public int staminaFallMulti;

    private int staminaRegen;
    public int staminaRegenMulti;

    private Rigidbody thisRigid;
    private float groundDist;

    private float fatigue = 12.0f;
    private bool rolling = false;

    private void Start()
    {
        // Identifies the player's rigidbody
        thisRigid = gameObject.GetComponent<Rigidbody>();

        // Check the distance to the ground
        groundDist = gameObject.GetComponent<CapsuleCollider>().bounds.extents.y;

        playerTransform = this.transform;
        cocoArr = this.transform.GetChild(1).GetComponentsInChildren<Transform>();

        stamina = maxStamina;
        staminaFall = 1;
        staminaRegen = 1;
    }
    
    private void FixedUpdate()
    {
        Debug.Log(stamina);
        
        PlayerMovement();

        RaycastHit hit;
        if (Physics.Raycast(playerTransform.position, playerTransform.forward, out hit, 5.0f))
        {
            if (hit.collider.gameObject.tag.Equals("Coconut") && Input.GetMouseButtonDown(0))
            {
                for (int i = 1; i < cocoArr.Length; i++)
                {
                    if (!cocoArr[i].gameObject.GetComponent<MeshRenderer>().enabled)
                    {
                        cocoArr[i].gameObject.GetComponent<MeshRenderer>().enabled = true;
                        hit.collider.gameObject.SetActive(false);
                        break;
                    }
                }
            }

            Interact(hit, hit.collider.gameObject.tag);
        }

        Debug.DrawRay(playerTransform.position, playerTransform.forward * 5, Color.green);
    }

    // Moves the player depending on the grounded condition
    private void PlayerMovement()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        Vector3 jump = new Vector3(h * 7.0f, jumpSpeed, v * 7.0f);
        Vector3 move = new Vector3(h, 0.0f, v) * speed;

        gameObject.GetComponent<Transform>().Translate(move / fatigue);

        try
        {
            if (hideController.isHolding() && !transform.GetChild(1).GetComponent<Animator>().GetBool("Roll"))
            {
                fatigue = 26.0f;
            }
            else if ((transform.GetChild(1).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CocoRollRevAnim")
                || transform.GetChild(1).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CocoRollAnim")))
            {
                fatigue = 12.0f;
            }

            if (v > 0 && transform.GetChild(1).GetComponent<Animator>() != null)
            {
                transform.GetChild(1).GetComponent<hideController>().startRoll(true, v);
            }
            else if (v < 0 && transform.GetChild(1).GetComponent<Animator>() != null)
            {
                transform.GetChild(1).GetComponent<hideController>().startRoll(true, v);
            }
            else
            {
                transform.GetChild(1).GetComponent<hideController>().startRoll(false, v);
            }
        }
        catch(MissingComponentException)
        {

        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        transform.GetChild(1).GetComponent<hideController>().downHill(true);
    }

    private void OnTriggerExit(Collider other)
    {
        transform.GetChild(1).GetComponent<hideController>().downHill(false);
        foreach (Transform t in transform.GetChild(1).GetComponentsInChildren<Transform>())
        {
            if (t.GetComponent<MeshCollider>() == null)
            {
                
            }
            else
            {
                t.GetComponent<MeshCollider>().enabled = true;
                t.GetComponent<Rigidbody>().isKinematic = false;
                transform.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.None;
            }
        }

        Destroy(transform.GetChild(1).GetComponent<Animator>());
        transform.GetChild(1).GetChild(0).parent = null;
        transform.GetChild(1).GetChild(0).parent = null;
    }

    private bool isGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, groundDist + 0.1f);
    }

    public Transform[] getCoco()
    {
        return cocoArr;
    }

    private void Interact(RaycastHit hit, string tag)
    {
        if (Input.GetMouseButtonDown(0))
        {
            switch (tag)
            {
                case "Box":
                    hit.collider.gameObject.GetComponent<Animator>().SetTrigger("Open");
                    break;

                case "Jar":
                    hit.collider.transform.GetChild(1).GetChild(0).GetComponent<Animator>().SetTrigger("Open");
                    Destroy(hit.collider);
                    break;

                case "Eel":
                    hit.collider.transform.GetComponent<Animator>().SetBool("Leave", true);
                    StartCoroutine(EelState(hit));
                    break;

                default:
                    Debug.Log("No tag - not interactable.");
                    break;
            }
        }
    }

    private IEnumerator EelState(RaycastHit hit)
    {
        yield return new WaitForSeconds(Time.deltaTime * 5.0f);
        hit.collider.transform.GetComponent<Animator>().SetBool("Leave", false);
    }
}
