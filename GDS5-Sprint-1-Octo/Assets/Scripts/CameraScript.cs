﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public float rotationSpeed = 1.0f;
    public Transform target, player, body;

    private float mouseX, mouseY;

    Vector3 velocity = Vector3.one;
    
    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
 
    private void FixedUpdate()
    {
        CamControl();
        RotationMode();
    }

    private void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -35, 60);

        Quaternion rotateTo = Quaternion.Euler(mouseY, mouseX, 0.0f);
        Quaternion curRotation = target.rotation;

        target.rotation = Quaternion.Lerp(curRotation, rotateTo, Time.deltaTime * 15);

        transform.LookAt(target);
    }

    private void RotationMode()
    {
        Quaternion curRotation = target.rotation;

        // USE THE FOLLOWING CODE IN AN "IF/ELSE" STATEMENT TO ENABLE INDEPENDENT CAMERA MOVEMENT
        if (Input.GetKey(KeyCode.LeftShift))
        {
            target.rotation = Quaternion.Lerp(curRotation, Quaternion.Euler(mouseY, mouseX, 0.0f), Time.deltaTime * 15);
        }
        else
        {
            player.rotation = Quaternion.Euler(0.0f, mouseX, 0.0f);
        }
    }
}
