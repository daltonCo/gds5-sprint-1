﻿Shader "Custom/SimpleToon"
{
    Properties
    {
		_Color("Color", Color) = (0.5, 0.65, 1, 1)
		_MainTex("Main Texture", 2D) = "white" {}

		[HDR]
		_AmbientCol("Ambient Colour", Color) = (0.9, 0.9, 0.9, 1)
    }
    SubShader
    {
        Pass
        {
			Tags
			{
			"LightMode" = "ForwardBase"
			"PassFlags" = "OnlyDirectional"
			}

            CGPROGRAM
            #pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

            struct appdata
            {
				float3 normal : NORMAL;
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float3 worldNormal : NORMAL;
				float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
                return o;
            }

			float4 _Color;
			float4 _AmbientCol;

            float4 frag (v2f i) : SV_Target
            {
				float3 normal = normalize(i.worldNormal);
				float NdotL = dot(_WorldSpaceLightPos0, normal);

				float lightInt = smoothstep(0, 0.01, NdotL);
				float4 light = lightInt * _LightColor0;

                // sample the texture
                float4 sample = tex2D(_MainTex, i.uv);

                return _Color * sample * (_AmbientCol + light);
            }
            ENDCG
        }
    }
}
