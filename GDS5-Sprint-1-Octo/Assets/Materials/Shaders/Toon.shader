﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Toon"
{
	Properties
	{
		_Color("Color", Color) = (0.5, 0.65, 1, 1)
		_MainTex("Main Texture", 2D) = "white" {}	
		
		[HDR]	// Best practice to use HDR for colours that represent lights
		_AmbientColor("Ambient Colour", Color) = (0.4, 0.4, 0.4, 1)

		[HDR]
		_SpecularColor("Specular Colour", Color) = (0.9, 0.9, 0.9, 1)
		_Glossiness("Glossiness", Float) = 15

		[HDR]
		_RimColor("Rim Colour", Color) = (1,1,1,1)
		_RimValue("Rim Value", Range(0, 1)) = 0.716
		_RimLimit("Rim Limit", Range(0, 1)) = 0.1
	}
	SubShader
	{
		Pass
		{
			Tags
			{
				"LightMode" = "ForwardBase"
				"PassFlags" = "OnlyDirectional"
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float3 normal : NORMAL;
				float4 vertex : POSITION;				
				float4 uv : TEXCOORD0;
			};

			struct v2f
			{
				SHADOW_COORDS(2)
				float4 pos : SV_POSITION;
				float3 worldNormal : NORMAL;
				float2 uv : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert (appdata v)
			{
				v2f o;
				
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.viewDir = WorldSpaceViewDir(v.vertex);
				o.pos = UnityObjectToClipPos(v.vertex);
				TRANSFER_SHADOW(o)
				
				return o;
			}

			float4 _Color;
			float4 _AmbientColor;

			float _Glossiness;
			float4 _SpecularColor;

			float4 _RimColor;
			float _RimValue;
			float _RimLimit;

			float4 frag (v2f i) : SV_Target
			{	
				float shadow = SHADOW_ATTENUATION(i);

				float3 normal = normalize(i.worldNormal);
				float3 viewDir = normalize(i.viewDir);
				float4 sample = tex2D(_MainTex, i.uv);

				float NdotL = dot(_WorldSpaceLightPos0, normal);

				float lightIntensity = smoothstep(0, 0.01, NdotL * shadow);
				float4 light = lightIntensity * _LightColor0;

				float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);
				float NdotH = dot(normal, halfVector);

				float specInt = pow(NdotH * lightIntensity, _Glossiness * _Glossiness);
				float specIntSmooth = smoothstep(0.005, 0.01, specInt);
				float4 spec = specIntSmooth * _SpecularColor;

				float4 rimDot = 1 - dot(viewDir, normal);

				float rimInt = rimDot * pow(NdotL, _RimLimit);
				rimInt = smoothstep(_RimValue - 0.01, _RimValue + 0.01, rimInt);
				float4 rim = rimInt * _RimColor;

				// float4 lightDim = (0, 0, 0, (1 - shadow));

				return _Color * sample * (_AmbientColor + light + spec + rim);
			}
			ENDCG
		}
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
	Fallback "Legacy Shaders/VertexLit"
}